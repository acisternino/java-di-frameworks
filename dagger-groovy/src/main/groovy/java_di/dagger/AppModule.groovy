package java_di.dagger

import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    static HelloService provideHelloService() {
        return new HelloService()
    }
}
