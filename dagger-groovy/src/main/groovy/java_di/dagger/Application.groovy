package java_di.dagger

import dagger.Component

@Component(modules = AppModule.class)
interface Application {
    ApplicationImpl application()
}
