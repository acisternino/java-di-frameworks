package java_di.dagger

import javax.inject.Inject

class ApplicationImpl {

    private HelloService service

    @Inject
    ApplicationImpl(HelloService service) {
        this.service = service
    }

    void run(String[] args) {
        println service.hello
    }
}
