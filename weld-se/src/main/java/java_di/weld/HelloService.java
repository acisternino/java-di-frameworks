package java_di.weld;

import javax.inject.Singleton;

@Singleton
public class HelloService
{
    public String getHello() {
        return "Hello world!";
    }
}
