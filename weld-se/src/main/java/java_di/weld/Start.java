package java_di.weld;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Start
{
    public static void main(String[] args) {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        Application app = container.instance().select(Application.class).get();
        app.run(args);
        container.shutdown();
    }
}
