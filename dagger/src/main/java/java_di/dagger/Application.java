package java_di.dagger;

import dagger.Component;

@Component(modules = AppModule.class)
public interface Application
{
    ApplicationImpl application();
}
