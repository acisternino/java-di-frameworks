package java_di.dagger;

public class Start
{
    public static void main(String[] args) {
        Application app = DaggerApplication.create();
        app.application().run(args);
    }
}
