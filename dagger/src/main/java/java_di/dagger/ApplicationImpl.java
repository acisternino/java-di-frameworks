package java_di.dagger;

import javax.inject.Inject;

public class ApplicationImpl
{
    private HelloService service;

    @Inject
    public ApplicationImpl(HelloService service) {
        this.service = service;
    }

    public void run(String[] args) {
        System.out.println(service.getHello());
    }
}
