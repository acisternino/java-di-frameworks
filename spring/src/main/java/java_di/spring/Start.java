package java_di.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Start
{
    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class)) {
            Application app = ctx.getBean(Application.class);
            app.run(args);
        }
    }
}
