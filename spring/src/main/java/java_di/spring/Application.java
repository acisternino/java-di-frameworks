package java_di.spring;

import org.springframework.beans.factory.annotation.Autowired;

public class Application
{
    private HelloService service;

    @Autowired
    public Application(HelloService service) {
        this.service = service;
    }

    public void run(String[] args) {
        System.out.println(service.getHello());
    }
}
