package java_di.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("java_di.spring")
public class AppConfig
{
    @Bean
    public Application application(HelloService service) {
        return new Application(service);
    }
}
