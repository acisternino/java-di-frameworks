package java_di.spring;

import org.springframework.stereotype.Service;

@Service
public class HelloService
{
    public String getHello() {
        return "Hello world!";
    }
}
