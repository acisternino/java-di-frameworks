package java_di.guice

import com.google.inject.AbstractModule
import com.google.inject.Singleton

class AppModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Application.class).in(Singleton.class)
    }
}
