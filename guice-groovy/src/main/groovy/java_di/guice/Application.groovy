package java_di.guice

import javax.inject.Inject

class Application {

    private HelloService service

    @Inject
    Application(HelloService service) {
        this.service = service
    }

    void run(String[] args) {
        println service.hello
    }
}
