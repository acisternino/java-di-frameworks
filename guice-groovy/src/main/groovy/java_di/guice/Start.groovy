package java_di.guice

import com.google.inject.Guice
import com.google.inject.Injector

def injector = Guice.createInjector(new AppModule())

injector.getInstance(Application.class).run(args)
