package java_di.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Start
{
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new AppModule());
        Application app = injector.getInstance(Application.class);
        app.run(args);
    }
}