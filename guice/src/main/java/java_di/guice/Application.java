package java_di.guice;

import javax.inject.Inject;

public class Application
{
    private HelloService service;

    @Inject
    public Application(HelloService service) {
        this.service = service;
    }

    public void run(String[] args) {
        System.out.println(service.getHello());
    }
}
